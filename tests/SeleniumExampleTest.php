<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modelizer\Selenium\SeleniumTestCase;

class SeleniumExampleTest extends SeleniumTestCase
{

    /**
     * A basic submission test example.
     *
     * @return void
     */
    public function testLoginFormExample()
    {

        // Login form test case scenario
        $this->visit('http://www.facebook.com')
            ->type('psgganesh@gmail.com', '#email')->hold(3)
            ->type('R@bbith0le', '#pass')->hold(3)
            ->click('Log In')->hold(10)
            ->type('psgganesh@gmail.com', '#email')->hold(3)
            ->type('R@bbith0le', '#pass')->hold(3)
            ->click('Log In')->hold(3)
            ->see('Facebook')->hold(3);  // Expected Result
    }
}
