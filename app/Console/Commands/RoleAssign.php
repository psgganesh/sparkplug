<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class RoleAssign extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'role:assign {--role=NULL} {--email=NULL}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Assign a role to an user';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $role   = $this->option('role');
        $user   = $this->fetchUser($this->option('email'));
        $user->assignRole(strtolower($role));
        $this->comment($user." is now ".$role.PHP_EOL);
    }

    public function fetchUser($user)
    {
        return is_null(User::where('email',$user)->first()) ? User::where('id',$user)->first() : User::where('email',$user)->first();
    }
}
