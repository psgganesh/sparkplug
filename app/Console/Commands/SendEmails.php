<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'emails:send {type} {name} {email} {--subject=Greetings from Sparkplug} {--token=NULL} {--caption=NULL} {--fullUrl=NULL} {--headers=NULL} {--parentClass=NULL} {--exceptionTrace=NULL}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send emails to a specified user';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name       = $this->argument('name');
        $email      = $this->argument('email');
        $subject    = $this->option('subject');
        $token      = $this->option('token');
        $caption    = $this->option('caption');
        $fullUrl    = $this->option('fullUrl');
        $headers    = $this->option('headers');
        $parentClass    = $this->option('parentClass');
        $exceptionTrace = $this->option('exceptionTrace');


        if(is_null($token)) {

            Mail::send('emails.'.$this->argument('type').'', [
                'name' => $name,
                'email' => $email,
                'subject' => $subject
            ], function ($mail) use ($name, $email, $subject) {
                $mail->subject($subject);
                $mail->to($email, $name);
            });

        } else {

            switch($this->argument('type'))
            {
                case "exception":
                    Mail::send('emails.'.$this->argument('type').'', [
                        'name' => $name,
                        'email' => $email,
                        'subject' => $subject,
                        'caption' => $caption,
                        'fullUrl' => $fullUrl,
                        'headers' => $headers,
                        'parentClass' => $parentClass,
                        'exceptionTrace' => $exceptionTrace,
                    ], function ($mail) use ($name, $email, $subject, $caption, $fullUrl, $headers, $parentClass, $exceptionTrace) {
                        $mail->subject($subject);
                        $mail->to($email, $name);
                    });
                    break;
                default:
                    Mail::send('emails.'.$this->argument('type').'', [
                        'name' => $name,
                        'email' => $email,
                        'token' => $token,
                        'subject' => $subject
                    ], function ($mail) use ($name, $email, $subject, $token) {
                        $mail->subject($subject);
                        $mail->to($email, $name);
                    });
            }
        }

    }
}
