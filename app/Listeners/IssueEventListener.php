<?php

namespace App\Listeners;

use App\Events\ErrorOccurredEvent;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;


class IssueEventListener
{

    protected $type;

    protected $emailToName;

    protected $emailToAddress;

    protected $appName;

    /**
     * Create the event listener.
     *
     */
    public function __construct()
    {
        $this->type = 'exception';
        $this->appName = env('APP_NAME', null);
        $this->emailToName = env('DEVELOPER_EMAIL');
        $this->emailToAddress = env('DEVELOPER_EMAIL');
    }

    /**
     * Handle the event.
     *
     * @param  ErrorOccurredEvent $event
     *
     */
    public function handle(ErrorOccurredEvent $event)
    {
        Log::error('Event occurred: '.$event->statusCode);
        $data['statusCode'] = $event->statusCode;

        $subject = $event->statusCode.' exception occurred in '.$this->appName.' project';
        $headers = $event->headers['user-agent'][0];

        Artisan::call('emails:send', [
            'type' => $this->type,
            'name' => $this->emailToName,
            'email' => $this->emailToAddress,
            '--subject' => $subject,
            '--caption' => $event->message,
            '--fullUrl' => $event->fullUrl,
            '--headers' => $headers,
            '--parentClass' => $event->parentClass,
            '--exceptionTrace' => $event->exception
        ]);

    }
}
