<?php

namespace App\Http\Middleware;

use Closure;

class MustBeSuperAdministrator
{
    protected $response;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->response = $next($request);
        $this->addResponseHeaders();
        $user = $request->user();
        if(!$user->hasRole('superadmin'))
        {
            abort(403);
        }
        return $this->response;
    }

    public function addResponseHeaders()
    {
        $this->response->header('Pragma', 'no-cache');
        $this->response->header('Expires', 'Fri, 01 Jan 1990 00:00:00 GMT');
        $this->response->header('Cache-Control', 'no-cache, must-revalidate, no-store, max-age=0, private');
    }

}
