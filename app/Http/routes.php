<?php

    /*
    |--------------------------------------------------------------------------
    | Routes File
    |--------------------------------------------------------------------------
    |
    | Here is where you will register all of the routes in an application.
    | It's a breeze. Simply tell Laravel the URIs it should respond to
    | and give it the controller to call when that URI is requested.
    |
    */


    /*
    |--------------------------------------------------------------------------
    | Base / front-end page routes
    |--------------------------------------------------------------------------
    | Here is where you will register all of the routes of the application,
    | which does not involve login / authentication to view the pages
    |
    */

    Route::get('/', function(){ return view('welcome'); });

    /*
    |--------------------------------------------------------------------------
    | Application Routes
    |--------------------------------------------------------------------------
    |
    | This route group applies the "web" middleware group to every route
    | it contains. The "web" middleware group is defined in your HTTP
    | kernel and includes session state, CSRF protection, and more.
    |
    */


    Route::group(['middleware' => 'web' ], function () {


        // Login, Registration, Forget password routes

        Route::auth();


        // Front-end routes
        

        // REST API controller routes

        Route::group(['prefix' => '/api/v1'], function () {

        });


    });

