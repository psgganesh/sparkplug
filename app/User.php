<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Role;

class User extends Authenticatable
{
    use HasRoles;

    protected $counter = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Return all users except the one who has logged in
     *
     * @param $userId
     * @return mixed
     */
    public function getUsersExceptLoggedInUser($userId)
    {
        return $this->where('id', '!=', $userId)->get();
    }

    /**
     * Return all users except the one who has logged in with role detail
     *
     * @param $userId
     * @return Collection
     */
    public function getUsersExceptLoggedInUserWithRoles($userId)
    {
        $users = $this->getUsersExceptLoggedInUser($userId);
        foreach($users as $user)
        {
            $users[$this->counter]['name'] = $user->name;
            $users[$this->counter]['email'] = $user->email;
            $users[$this->counter]['role'] = $user->roles->implode('name', ', ');
            $this->counter++;
        }
        return $users;
    }

    /**
     * Return user object
     *
     * @param $id
     * @return mixed
     */
    public function getUserInfoWithRole($id)
    {
        $user = $this->find($id);
        return $user;
    }

    /**
     * Return very first role of the user object
     *
     * @param $id
     * @return mixed
     */
    public function getRole($id)
    {
        $currentRole  = current(current($this->find($id)->roles));
        return $currentRole->role;
    }

    /**
     * Send the password reset notification.
     *
     * @param  string $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        // TODO: Implement sendPasswordResetNotification() method.
    }

}
