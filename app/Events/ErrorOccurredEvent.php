<?php

namespace App\Events;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ErrorOccurredEvent extends Event
{
    use SerializesModels;

    public $headers;

    public $message;

    public $code;

    public $file;

    public $line;

    public $fullUrl;

    public $trace;

    public $parentClass;

    public $exception;

    public $statusCode;

    /**
     * Create a new event instance.
     *
     * @param Exception $exception
     * @param Request $request
     * @param int $statusCode
     */
    public function __construct(Exception $exception, Request $request, $statusCode)
    {
        $this->message = $exception->getMessage();
        $this->fullUrl = $request->fullUrl();
        $this->headers = $request->header();
        $this->parentClass = get_class($exception);
        $this->exception = $exception->getTraceAsString();
        $this->statusCode = $statusCode;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
