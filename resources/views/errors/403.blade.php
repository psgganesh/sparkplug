<!DOCTYPE html>
<html>
    <head>
        <title>Oops! Un-authorized action</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:400" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #2C3E50;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 72px;
                margin-bottom: 40px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Oops! 403 Un-authorized action</div>
                <p class="text-muted">Currently you do not have sufficent access to view this page, please contact web-admin if required.</p>
            </div>
        </div>
    </body>
</html>
