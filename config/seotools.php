<?php

return [
    'meta'      => [
        /*
         * The default configurations to be used by the meta generator.
         */
        'defaults'       => [
            'title'        => env('APP_TITLE', 'Laravel'), // set false to total remove
            'description'  => env('SEO_TOOLS_DESCRIPTION','A brief description of the project'), // set false to total remove
            'separator'    => env('SEO_TOOLS_TITLE_SEPERATOR', ' - '),
            'keywords'     => [env('SEO_TOOLS_KEYWORDS','test, keywords')],
            'canonical'    => env('SEO_TOOLS_CANONICAL', false), // Set null for using Url::current(), set false to total remove
        ],

        /*
         * Webmaster tags are always added.
         */
        'webmaster_tags' => [
            'google'    => env('SEO_TOOLS_GOOGLE_TAG',null),
            'bing'      => env('SEO_TOOLS_BING_TAG',null),
            'alexa'     => env('SEO_TOOLS_ALEXA_TAG',null),
            'pinterest' => env('SEO_TOOLS_PINTEREST_TAG',null),
            'yandex'    => env('SEO_TOOLS_YANDEX_TAG',null),
        ],
    ],
    'opengraph' => [
        /*
         * The default configurations to be used by the opengraph generator.
         */
        'defaults' => [
            'title'       => env('SEO_TOOLS_OPENGRAPH_TITLE', 'Sparkplug Open graph title'), // set false to total remove
            'description' => env('SEO_TOOLS_OPENGRAPH_DESCRIPTION', 'Sparkplug Open graph description'), // set false to total remove
            'url'         => env('SEO_TOOLS_OPENGRAPH_URL', false),
            'type'        => env('SEO_TOOLS_OPENGRAPH_TYPE', false),
            'site_name'   => env('SEO_TOOLS_OPENGRAPH_SITE_NAME', false),
            'images'      => [],
        ],
    ],
    'twitter' => [
        /*
         * The default values to be used by the twitter cards generator.
         */
        'defaults' => [
          'card'        => env('SEO_TOOLS_TWITTER_CARD', 'app'),
          'site'        => env('SEO_TOOLS_TWITTER_SITE', '@psgganesh'),
        ],
    ],
];
