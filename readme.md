# Sparkplug PHP Framework

[![Total Downloads](https://poser.pugx.org/psgganesh/sparkplug/d/total.svg)](https://packagist.org/packages/psgganesh/sparkplug)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Sparkplug is a web application framework built on top of laravel. We attempt in making web development more modular using laravel and composer packages; aiming to take web application development as simple as possible.

## License

The Sparkplug framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
