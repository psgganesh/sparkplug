<?php

return [

    'navbar' => [

        'login' => 'Login',
        'register' => 'Register',
        'toggleNavigation' => 'Toggle Navigation',
        'userNRoles' => 'Users &amp; Roles',
        'environment' => 'Environment',
        'frontend' => 'Frontend',
        'logout' => 'Logout'

    ],

    'acl' => [

        'navTitle' => 'Access Control',
        'users' => 'Users',
        'roles' => 'Roles',
        'permissions' => 'Permissions',
        'assign' => 'Assign',
        'access' => 'Access'

    ],

    'alerts' => [

        'noPermissionTitle' => 'No Permission',
        'noPermissionBody' => 'Sorry, but it looks like you / the logged in user has no permissions to do any action on this page!',

    ],

    'users' => [

        'form' => [
            'create' => [
                'modalTitle' => 'Create new user',
                'email' => 'Email',
                'name' => 'Name',
                'password' => 'Password',
                'closeButton' => 'Close',
                'submitButton' => 'Submit'
            ],
            'edit' => [
                'modalTitle' => 'Edit user',
                'email' => 'Email',
                'name' => 'Name',
                'password' => 'Password',
                'closeButton' => 'Close',
                'submitButton' => 'Update user'
            ]
        ],

        'grid' => [
            'search' => [
                'id' => 'Filter by #',
                'name' => 'Filter by name',
                'role' => 'Filter by role',
                'email' => 'Filter by email',
                'created_at' => 'Filter by created timestamp',
                'updated_at' => 'Filter by updated timestamp',
            ],
            'panel' => [
                'createNewRecord' => 'Create new user',
                'csvExportLink' => 'CSV Export',
                'xlsExportLink' => 'XLS Export',
                'trashRecordsLink' => 'Trash Records'
            ],
            'column' => [
                'id' => '#',
                'name'=> 'Name',
                'role' => 'Role',
                'email' => 'Email',
                'created_at' => 'Created At',
                'updated_at' => 'Updated At',
                'actions' => 'Actions'
            ]
        ]

    ],

    'roles' => [

        'form' => [
            'create' => [
                'modalTitle' => 'Create new role',
                'name' => 'Role',
                'closeButton' => 'Close',
                'submitButton' => 'Submit'
            ],
            'edit' => [
                'modalTitle' => 'Edit role',
                'name' => 'Role',
                'closeButton' => 'Close',
                'submitButton' => 'Update role'
            ]
        ],

        'grid' => [
            'search' => [
                'id' => 'Filter by #',
                'name' => 'Filter by role'
            ],
            'panel' => [
                'createNewRecord' => 'Create new role',
                'csvExportLink' => 'CSV Export',
                'xlsExportLink' => 'XLS Export',
                'trashRecordsLink' => 'Trash Records'
            ],
            'column' => [
                'id' => '#',
                'name' => 'Role',
                'actions' => 'Actions'
            ]
        ]

    ],

    'permissions' => [

        'form' => [
            'create' => [
                'modalTitle' => 'Create new permission',
                'name' => 'Permission',
                'closeButton' => 'Close',
                'submitButton' => 'Submit'
            ],
            'edit' => [
                'modalTitle' => 'Edit permission',
                'name' => 'Permission',
                'closeButton' => 'Close',
                'submitButton' => 'Update permission'
            ]
        ],

        'grid' => [
            'search' => [
                'id' => 'Filter by #',
                'name' => 'Filter by permission'
            ],
            'panel' => [
                'createNewRecord' => 'Create new permission',
                'csvExportLink' => 'CSV Export',
                'xlsExportLink' => 'XLS Export',
                'trashRecordsLink' => 'Trash Records'
            ],
            'column' => [
                'id' => '#',
                'name' => 'Permission',
                'actions' => 'Actions'
            ]
        ]

    ],

    'assign' => [

        'grid' => [
            'search' => [
                'id' => 'Filter by #',
                'name' => 'Filter by name',
                'role' => 'Filter by role',
                'email' => 'Filter by email',
                'created_at' => 'Filter by created timestamp',
                'updated_at' => 'Filter by updated timestamp',
            ],
            'panel' => [
                'enable' => [
                    'admin' => 'Assign as Admin',
                    'superadmin' => 'Assign as Webmaster',
                    'webmaster' => 'Assign as Superadmin',
                ],
                'disable' => [
                    'admin' => 'Revoke Admin role',
                    'superadmin' => 'Revoke Webmaster role',
                    'webmaster' => 'Revoke Superadmin role',
                ]
            ],
            'column' => [
                'id' => '#',
                'name'=> 'Name',
                'role' => 'Role',
                'email' => 'Email',
                'created_at' => 'Created At',
                'updated_at' => 'Updated At',
                'actions' => 'Actions'
            ]
        ]

    ]




];