@extends('admin::template.default-navigation')

@section('admin::content')

    @include('admin::partials.control-data-list', [
        'module'            => 'assign',
        'model'             => $model,
        'apiSlug'           => 'superadmin/api/v1/',
        'tableClass'        => 'table-striped table-hover table-bordered m-b-0',
        'paginate'          => 0,
        'columnFilters'     => false,
        'actions'           => false,
        'dependentColumn'   => 'role',
        'options'           => $roles
    ])

@endsection


@section('admin::page_scripts')
    <script type="application/javascript">

        app.controller('controlController', function ($http, $rootScope, $scope, API_URL) {

            $scope.enableControl = function (roleName) {

                var checkedValues = $('input:checkbox:checked').map(function () {
                    if(this.value != "on") {
                        return this.value;
                    }
                }).get();

                $http({

                    url: API_URL,
                    method: 'POST',
                    data: {
                        action: 'assign',
                        selectedUsers: checkedValues.toString(),
                        roleName: roleName
                    }

                }).then(function (res) {

                    console.log(res.data.data);

                }, function (error) {
                    console.log(error);
                });

                $scope.refreshView();
            };

            $scope.disableControl = function (roleName) {

                var checkedValues = $('input:checkbox:checked').map(function () {
                    if(this.value != "on") {
                        return this.value;
                    }
                }).get();

                $http({

                    url: API_URL,
                    method: 'POST',
                    data: {
                        action: 'revoke',
                        selectedUsers: checkedValues.toString(),
                        roleName: roleName
                    }

                }).then(function (res) {

                    console.log(res.data.data);

                }, function (error) {
                    console.log(error);
                });

                $scope.refreshView();
            };

            $scope.refreshView = function () {
                $("#checkAll").attr('checked', false);
                $rootScope.getAllRecords();
            };
        });

    </script>
@stop

