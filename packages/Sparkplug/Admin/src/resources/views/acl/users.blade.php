@extends('admin::template.default-navigation')

@section('admin::content')

    @include('admin::partials.data-list', [
        'module'            => 'users',
        'model'             => $model,
        'apiSlug'           => 'superadmin/api/v1/',
        'tableClass'        => 'table-striped table-hover table-bordered m-b-0',
        'paginate'          => 12,
        'columnFilters'     => true,
        'actions'           => true,
        'create'            => [ 'modal' => true, 'default' => true, 'labelClass' => 'col-lg-2', 'fieldClass' => 'col-lg-10' ],
        'view'              => [ 'modal' => false ],
        'edit'              => [ 'modal' => true, 'default' => true, 'labelClass' => 'col-lg-2', 'fieldClass' => 'col-lg-10' ],
        'delete'            => [ 'softDeletes' => false ]
    ])

@endsection


@section('admin::page_scripts')
    <script type="application/javascript">

        app.controller('listController', function ($http, $rootScope, $scope, API_URL) {

            $scope.showRecord = function ($recordId) {};

            $scope.trashRecord = function ($recordId) {};

            $scope.editRecord = function ($recordId) {

                $("#editUsersForm").attr('action', API_URL + '/' + $recordId);
                $http.get(API_URL + '/' + $recordId).then(function (response) {
                    $scope.editRecordDataCollection = response.data.data;
                    $("#editName").val($scope.editRecordDataCollection.name);
                    $("#editEmail").val($scope.editRecordDataCollection.email);
                    console.log(response.data.data);
                });

            };

        });

    </script>
@stop