<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 p-l-0 p-r-0">
            <div ng-app="{{ $module }}">
                <div ng-controller="crudController">
                    <div class="panel panel-default m-b-0">
                        <div class="panel-heading" ng-controller="controlController">
                            @can('perform bulk operations on '.$module)
                                <div id="bulkOperationsDropDown" class="btn-group btn-group-sm hidden-sm hidden-xs" role="group" aria-label="...">
                                    @can('enable '.$module.' record')
                                        @foreach($options as $optionItem)
                                            <button type="button" class="btn btn-default btn-sm" ng-click="enableControl('{{ $optionItem }}')"><i class="fa fa-check"></i> {{ trans('admin::admin.'.$module.'.grid.panel.enable.'.$optionItem) }}</button>
                                        @endforeach
                                    @endcan
                                    @can('disable '.$module.' record')
                                        @foreach($options as $optionItem)
                                            <button type="button" class="btn btn-danger btn-sm" ng-click="disableControl('{{ $optionItem }}')"><i class="fa fa-remove"></i> {{ trans('admin::admin.'.$module.'.grid.panel.disable.'.$optionItem) }}</button>
                                        @endforeach
                                    @endcan
                                </div>
                            @endcan
                        </div>
                        <div class="panel-body no-padding">
                            <div class="table-responsive">
                                <table st-table="displayedCollection" st-safe-src="rowCollection" class="table smart-table {{ $tableClass }}" ng-cloak>
                                    <thead>
                                    <tr class="table-header">
                                        <th class="text-center hidden-sm hidden-xs"><input type="checkbox" id="checkAll"/></th>
                                        @foreach($columns as $column => $dataType)
                                            @if($dataType != 'timestamp')
                                                <th st-sort="{{ $column }}">{{ trans('admin::admin.'.$module.'.grid.column.'.$column) }}</th>
                                            @else
                                                <th st-sort="{{ $column }}" class="hidden-sm hidden-xs">{{ trans('admin::admin.'.$module.'.grid.column.'.$column) }}</th>
                                            @endif
                                        @endforeach
                                        @if($actions)
                                            <th class="text-center" colspan="3">{{ trans('admin::admin.'.$module.'.grid.column.actions') }}</th>
                                        @endif
                                    </tr>
                                    @if($columnFilters)
                                        <tr>
                                            <th class="text-center hidden-sm hidden-xs"></th>
                                            @foreach($columns as $column => $dataType)
                                                @if($dataType == 'increments')
                                                    <th class="text-center hidden-sm hidden-xs"></th>
                                                @elseif($dataType != 'timestamp')
                                                    <th>
                                                        <input st-search="{{ $column }}" placeholder="{{ trans('admin::admin.'.$module.'.grid.search.'.$column) }}" class="input-sm form-control" type="search"/>
                                                    </th>
                                                @else
                                                    <th class="hidden-sm hidden-xs">
                                                        <input st-search="{{ $column }}" placeholder="{{ trans('admin::admin.'.$module.'.grid.search.'.$column) }}" class="input-sm form-control" type="search"/>
                                                    </th>
                                                @endif
                                            @endforeach
                                            @if($actions)
                                                <th colspan="3"></th>
                                            @endif
                                        </tr>
                                    @endif
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="row in displayedCollection" ng-cloak>
                                            <td cs-select="row" class="text-center hidden-sm hidden-xs"></td>
                                            @foreach($columns as $column => $dataType)
                                                @if($dataType != 'timestamp')
                                                    @if($dataType == 'email')
                                                        <td><img gravatar-src="row.email"  width="20px" class="img-circle"> [[ row.{{ $column }} ]]</td>
                                                    @elseif($dataType != 'increments')
                                                        <td>[[ row.{{ $column }} ]]</td>
                                                    @else
                                                        <td>[[ $index+1 ]]</td>
                                                    @endif
                                                @else
                                                    <td class="hidden-sm hidden-xs">[[ row.{{ $column }} | datetime  ]]</td>
                                                @endif
                                            @endforeach
                                            @if($actions)
                                                @can('edit existing '.$module)
                                                    <td class="text-center">

                                                    </td>
                                                @endcan
                                            @endif
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        @if($paginate > 0)
                                            <td colspan="{{ count($columns) + 4 }}" class="text-right">
                                                <div st-pagination="" st-items-by-page="itemsByPage" st-displayed-pages=""></div>
                                            </td>
                                        @endif
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('admin::module_scripts')

    <script type="application/javascript">

        /**
         * Module: CRUD
         * Description: Angular JS Module for companies table CRUD
         * Developer: Shankar
         * Version: 0.0.1
         * Dated: 22-Nov-2016
         *
         * Dependencies
         * -----------------------------------------------------
         * jQuery, bootstrap.min.js, angularjs, smart-table
         *
         **/

        app = angular.module('{{ $module }}', ['smart-table', 'ui.gravatar']);

        app.constant('API_URL', '/{{ $apiSlug.$module }}');

        /**
         * Parent CRUD controller responsible to carry on REST calls
         **/
        app.controller('crudController', function ($http, $rootScope, $scope, API_URL) {

            $scope.itemsByPage = parseInt({{ (int)$paginate }});

        });

    </script>

    <script type="text/javascript" src="{{ URL::asset('/vendor/admin/assets/js/angular-utilities.js') }}" ></script>

@stop