<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 p-l-0 p-r-0">

            <nav class="navbar navbar-inverse bg-black m-b-0 b-r-0">
                <div class="collapse navbar-collapse p-l-0" id="bs-example-navbar-collapse-2">
                    <ul class="nav navbar-nav navbar-left">
                        <li><a href="#" class="text-uppercase"><i class="icon-people"></i> {{ trans('admin::admin.'.$trans.'.'.$parent) }}</a></li>
                    </ul>
                </div>
            </nav>

            <ul class="nav nav-tabs navbar-inverse m-b-0 navbar-inverse-tabs">

                @foreach($children as $child)
                    @can('access '.$child)
                        <li class="{{ is_active(''.$child.'') }} text-uppercase"><a href="{{ URL::to('/superadmin/'.$child) }}" class="b-r-0">{{ trans('admin::admin.'.$trans.'.'.$child) }}</a></li>
                    @endcan
                @endforeach

            </ul>

            <div class="tab-content">

                <!-- Content Wrapper. Contains page content -->
                @include('admin::partials.content')

            </div>

        </div>
    </div>
</div>