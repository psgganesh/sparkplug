<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 p-l-0 p-r-0">
            <div ng-app="{{ $module }}">
                <div ng-controller="crudController">
                    <div class="panel panel-default m-b-0">
                        <div class="panel-heading">
                            @can('create new '.$module)
                                <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#createRecordModal"><i class="fa fa-plus"></i> {{ trans('admin::admin.'.$module.'.grid.panel.createNewRecord') }}</button>
                            @else
                                <a class="btn btn-danger btn-sm link-disabled" ><i class="fa fa-plus"></i> {{ trans('admin::admin.'.$module.'.grid.panel.createNewRecord') }}</a>
                            @endcan
                            @can('perform bulk operations on '.$module)
                                <div id="bulkOperationsDropDown" class="btn-group btn-group-sm pull-right hidden-sm hidden-xs" role="group" aria-label="...">
                                    @can('delete '.$module)
                                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash"></i> {{ trans('admin::admin.'.$module.'.grid.panel.trashRecordsLink') }}</button>
                                    @endcan
                                </div>
                            @endcan
                        </div>
                        <div class="panel-body no-padding">
                            <div class="table-responsive">
                                <table st-table="displayedCollection" st-safe-src="rowCollection" class="table smart-table {{ $tableClass }}" ng-cloak>
                                    <thead>
                                    <tr class="table-header">
                                        <th class="text-center hidden-sm hidden-xs"><input type="checkbox" id="checkAll"/></th>
                                        @foreach($columns as $column => $dataType)
                                            @if($dataType != 'timestamp')
                                                <th st-sort="{{ $column }}">{{ trans('admin::admin.'.$module.'.grid.column.'.$column) }}</th>
                                            @else
                                                <th st-sort="{{ $column }}" class="hidden-sm hidden-xs">{{ trans('admin::admin.'.$module.'.grid.column.'.$column) }}</th>
                                            @endif
                                        @endforeach
                                        @if($actions)
                                            <th class="text-center" colspan="3">{{ trans('admin::admin.'.$module.'.grid.column.actions') }}</th>
                                        @endif
                                    </tr>
                                    @if($columnFilters)
                                        <tr>
                                            <th class="text-center hidden-sm hidden-xs"></th>
                                            @foreach($columns as $column => $dataType)
                                                @if($dataType == 'increments')
                                                    <th class="text-center hidden-sm hidden-xs"></th>
                                                @elseif($dataType != 'timestamp')
                                                    <th>
                                                        <input st-search="{{ $column }}" placeholder="{{ trans('admin::admin.'.$module.'.grid.search.'.$column) }}" class="input-sm form-control" type="search"/>
                                                    </th>
                                                @else
                                                    <th class="hidden-sm hidden-xs">
                                                        <input st-search="{{ $column }}" placeholder="{{ trans('admin::admin.'.$module.'.grid.search.'.$column) }}" class="input-sm form-control" type="search"/>
                                                    </th>
                                                @endif
                                            @endforeach
                                            @if($actions)
                                                <th colspan="3"></th>
                                            @endif
                                        </tr>
                                    @endif
                                    </thead>
                                    <tbody ng-controller="listController">
                                        <tr ng-repeat="row in displayedCollection" ng-cloak>
                                            <td cs-select="row" class="text-center hidden-sm hidden-xs"></td>
                                            @foreach($columns as $column => $dataType)
                                                @if($dataType != 'timestamp')
                                                    @if($dataType == 'email')
                                                        <td><img gravatar-src="row.email"  width="20px" class="img-circle"> [[ row.{{ $column }} ]]</td>
                                                    @elseif($dataType != 'increments')
                                                        <td>[[ row.{{ $column }} ]]</td>
                                                    @else
                                                        <td>[[ row.id ]]</td>
                                                    @endif
                                                @else
                                                    <td class="hidden-sm hidden-xs">[[ row.{{ $column }} | datetime  ]]</td>
                                                @endif
                                            @endforeach
                                            @if($actions)

                                                @can('view '.$module.' detail')
                                                    <td class="text-center">
                                                        @if($view['modal'])
                                                            <a ng-click="showRecord(row.id)"><i class="fa fa-eye"></i></a>
                                                        @else
                                                            <a href="{{ URL::to($apiSlug.$module) }}/[[row.id]]"><i class="fa fa-eye"></i></a>
                                                        @endif
                                                    </td>
                                                @endcan

                                                @can('edit existing '.$module)
                                                    <td class="text-center">
                                                        @if($edit['modal'])
                                                            @if($edit['default'])
                                                                <a data-toggle="modal" data-target="#editRecordModal" ng-click="editRecord(row.id)"><i class="fa fa-pencil"></i></a>
                                                            @else
                                                                <a ng-click="editRecord(row.id)"><i class="fa fa-pencil"></i></a>
                                                            @endif
                                                        @else
                                                            <a href="{{ URL::to($apiSlug.$module) }}/[[row.id]]/edit/"><i class="fa fa-pencil"></i></a>
                                                        @endif
                                                    </td>
                                                @endcan

                                                @can('delete '.$module)
                                                    <td class="text-center">
                                                    @if($delete['softDeletes'])
                                                        <a ng-click="trashRecord(row.id)"><i class="fa fa-trash"></i></a>
                                                    @else
                                                        <a ng-click="deleteRecord(row.id)"><i class="fa fa-trash"></i></a>
                                                    @endif
                                                    </td>
                                                @endcan

                                            @endif
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        @if($paginate > 0)
                                            <td colspan="{{ count($columns) + 4 }}" class="text-right">
                                                <div st-pagination="" st-items-by-page="itemsByPage" st-displayed-pages=""></div>
                                            </td>
                                        @endif
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            @if($actions)
                <!-- CREATE RECORD STARTS HERE -->
                @if($create['default'])
                    <div class="modal fade" tabindex="-1" role="dialog" id="createRecordModal">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">{{ trans('admin::admin.'.$module.'.form.create.modalTitle') }}</h4>
                                </div>
                                {!! Form::open(['id' => 'store'.ucfirst($module).'Form' ,'route' => str_replace("/",".",$apiSlug).$module.'.store', 'method' => 'POST', 'class' => 'form-horizontal', 'files'=>true, 'autocomplete' => 'off', 'data-toggle' => 'validator', 'role' => 'form']) !!}
                                    <div class="modal-body">

                                        {{--TODO: Make this a form partial with switch case for each form field type--}}
                                        @foreach($model->getFillable() as $property)
                                            <div class="form-group">
                                                <div class="{{ $create['labelClass'] }}">
                                                    {{ Form::label($property, trans('admin::admin.'.$module.'.form.create.'.$property)) }}
                                                </div>
                                                <div class="{{ $create['fieldClass'] }}">
                                                    @if($property == 'password')
                                                        {{ Form::password($property, ['class' => 'form-control input-sm']) }}
                                                    @else
                                                        {{ Form::text($property, null, ['class' => 'form-control input-sm']) }}
                                                    @endif
                                                </div>
                                            </div>
                                        @endforeach

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">{{ trans('admin::admin.'.$module.'.form.create.closeButton') }}</button>
                                        <button type="submit" class="btn btn-primary btn-sm">{{ trans('admin::admin.'.$module.'.form.create.submitButton') }}</button>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                    {!! JsValidator::formRequest('Sparkplug\Admin\Http\Requests\CreateNew'.ucfirst($module).'Request', '#store'.ucfirst($module).'Form') !!}
                @endif
                <!-- CREATE RECORD ENDS HERE -->


                <!-- EDIT RECORD STARTS HERE -->
                @if($edit['default'])
                    <div class="modal fade" tabindex="-1" role="dialog" id="editRecordModal">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">{{ trans('admin::admin.'.$module.'.form.edit.modalTitle') }}</h4>
                                </div>
                                {!! Form::open(['id' => 'edit'.ucfirst($module).'Form' , 'method' => 'PATCH', 'class' => 'form-horizontal', 'files'=>true, 'autocomplete' => 'off', 'data-toggle' => 'validator', 'role' => 'form']) !!}
                                <div class="modal-body">

                                    {{--TODO: Make this a form partial with switch case for each form field type--}}
                                    @foreach($model->getFillable() as $property)
                                        <div class="form-group">
                                            <div class="{{ $edit['labelClass'] }}">
                                                {{ Form::label($property, trans('admin::admin.'.$module.'.form.edit.'.$property)) }}
                                            </div>
                                            <div class="{{ $edit['fieldClass'] }}">
                                                @if($property == 'password')
                                                    {{ Form::password($property, ['class' => 'form-control input-sm', 'id' => 'edit'.ucfirst($property) ]) }}
                                                @else
                                                    {{ Form::text($property, null, ['class' => 'form-control input-sm', 'id' => 'edit'.ucfirst($property) ]) }}
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">{{ trans('admin::admin.'.$module.'.form.create.closeButton') }}</button>
                                        <button type="submit" class="btn btn-primary btn-sm">{{ trans('admin::admin.'.$module.'.form.create.submitButton') }}</button>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    {!! JsValidator::formRequest('Sparkplug\Admin\Http\Requests\Edit'.ucfirst($module).'Request', '#edit'.ucfirst($module).'Form') !!}
                @endif
                <!-- EDIT RECORD ENDS HERE -->
            @endif
            </div>
        </div>
    </div>
</div>

@section('admin::module_scripts')

    <script type="application/javascript">

        /**
         * Module: CRUD
         * Description: Angular JS Module for companies table CRUD
         * Developer: Shankar
         * Version: 0.0.1
         * Dated: 22-Nov-2016
         *
         * Dependencies
         * -----------------------------------------------------
         * jQuery, bootstrap.min.js, angularjs, smart-table
         *
         **/

        app = angular.module('{{ $module }}', ['smart-table', 'ui.gravatar']);

        app.constant('API_URL', '/{{ $apiSlug.$module }}');

        /**
         * Parent CRUD controller responsible to carry on REST calls
         **/
        app.controller('crudController', function ($http, $rootScope, $scope, API_URL) {

            $scope.itemsByPage = parseInt({{ (int)$paginate }});

        });

    </script>

    <script type="text/javascript" src="{{ URL::asset('/vendor/admin/assets/js/angular-utilities.js') }}" ></script>

@stop