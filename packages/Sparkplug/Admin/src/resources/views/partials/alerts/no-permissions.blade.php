
    <div class="alert alert-danger b-r-0">
        <h4>{{ trans('alerts.noPermissionTitle') }}</h4>
        <p>{{ trans('alerts.noPermissionBody') }}</p>
    </div>
