<nav class="navbar navbar-default navbar-static-top m-b-0">
    <div class="container-fluid">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">{{ trans('admin::admin.navbar.toggleNavigation') }}</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/superadmin') }}">
                {{ getenv('APP_NAME') }}
            </a>
        </div>


        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}"> {{ trans('admin::admin.navbar.login') }}</a></li>
                    <li><a href="{{ url('/register') }}"> {{ trans('admin::admin.navbar.register') }}</a></li>
                @else

                    <li class="{{ is_active(['users','roles','permissions','assign','access']) }}" ><a href="{{ URL::to('/superadmin/users') }}" ><i class="icon-people"></i> {{ trans('admin::admin.navbar.userNRoles') }}</a></li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <i class="icon-user"></i> {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('/') }}"><i class="icon-home"></i> {{ trans('admin::admin.navbar.frontend') }}</a></li>
                            <li class="divider"></li>
                            <li><a href="{{ url('/logout') }}"><i class="icon-logout"></i> {{ trans('admin::admin.navbar.logout') }}</a></li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>