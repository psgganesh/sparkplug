@extends('admin::template.default')

@section('admin::content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 p-l-0 p-r-0">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group input-group-sm">
                            <div class="input-group-btn search-panel">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <span id="search_concept">Filter by</span> <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#contains">Contains</a></li>
                                    <li><a href="#its_equal">It's equal</a></li>
                                    <li><a href="#greather_than">Greather than ></a></li>
                                    <li><a href="#less_than">Less than < </a></li>
                                    <li class="divider"></li>
                                    <li><a href="#all">Anything</a></li>
                                </ul>
                            </div>
                            <input type="hidden" name="search_param" value="all" id="search_param">
                            <input type="text" class="form-control" name="x" placeholder="Search term...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button"><span class="icon-magnifier"></span></button>
                            </span>
                        </div>
                    </div>
                </div>
                </div>
                <div class="panel-body no-padding">

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
