@extends('admin::app')

@section('admin::template')

    <!-- Main Header -->
    @include('admin::partials.navbar')

    <!-- Secondary navigation -->
    @include('admin::partials.secondary-navigation',['parent' => $parent,'children' => $children])

@stop