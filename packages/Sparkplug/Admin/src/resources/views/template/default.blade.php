@extends('admin::app')

@section('admin::template')

    <!-- Main Header -->
    @include('admin::partials.navbar')

    <!-- Content Wrapper. Contains page content -->
    @include('admin::partials.content')

@stop