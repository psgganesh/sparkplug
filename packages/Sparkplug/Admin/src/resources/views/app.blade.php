<!DOCTYPE html>
<html lang="{{ getenv('DEFAULT_LOCALE') }}">
<head>

    <title>{{ getenv('APP_TITLE') }}</title>
    <meta name="description" content="{{ getenv('SEO_TOOLS_DESCRIPTION') }}">
    <meta name="keywords" content="{{ getenv('SEO_TOOLS_KEYWORDS') }}">

    <!-- Styles -->
    <link href="{{ asset('/vendor/admin/assets/css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/vendor/admin/assets/css/all.css') }}" crossorigin="anonymous">

    <!-- JavaScripts -->
    <script src="{{ asset('/vendor/admin/assets/js/all.js') }}"></script>
    <script src="{{ asset('/vendor/jsvalidation/js/jsvalidation.min.js') }}"></script>

</head>
<body id="app-layout">

    @yield('admin::template')

    @yield('admin::module_scripts')

    @yield('admin::page_scripts')

    @yield('admin::extra_scripts')

</body>
</html>
