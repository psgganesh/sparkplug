
    /**
     * Angular Utilities and re-usable scripts
     */

    /**
     * Overriding angular { { } } syntax to [ [ ] ]
     * for all variable value outputs in HTML
     **/

    app.config(['$interpolateProvider', function ($interpolateProvider) {

        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');

    }]);

    app.run(function($http, $rootScope, API_URL) {

        $rootScope.rowCollection = [];
        $rootScope.softDeletes = false;

        $rootScope.getAllRecords = function() {

            $http.get(API_URL).then(function (response) {

                $rootScope.rowCollection = response.data.data;

                console.log(response.data.data);

            });
        };

        $rootScope.deleteRecord = function ($recordId) {

            $http({

                url: API_URL + '/' + $recordId,
                method: 'DELETE'

            }).then(function (res) {

                console.log(res.data.data);

                $rootScope.getAllRecords();

            }, function (error) {

                console.log(error);
            });

        };

        $rootScope.bulkActions = function (action) {

            if (action === "delete") {

                var checkedValues = $('input:checkbox:checked').map(function () {
                    return this.value;
                }).get();

                $rootScope.deleteRecord(checkedValues);

            }

            // Un-check the check-all checkbox
            $("#checkAll").prop('checked', null);

        };

        $rootScope.getAllRecords();

    });

    /**
     * App filters
     */
    app.filter('capitalize', function() {
        return function(token) {
            return token.charAt(0).toUpperCase() + token.slice(1);
        }
    });

    app.filter('booleanFilter', function() {
        return function(token) {
            return token ? 'Yes' : 'No' ;
        }
    });

    app.filter('datetime', function($filter) {
        return function(input) {

            if(input == null){ return ""; }

            var _date = $filter('date')(new Date(input), 'MMM dd yyyy - HH:mm:ss');

            return _date.toUpperCase();

        };
    });

    app.filter('IST', function($filter) {
        return function(input) {

            if(input == null){ return ""; }

            var _date = $filter('date')(new Date(input), 'dd-MMM-yyyy');

            return _date.toUpperCase();

        };
    });

    /**
     * App directives
     */
    app.directive('csSelect', function () {
        return {
            require: '^stTable',
            template: checkBoxMultiSelectTemplate(),
            scope: {
                row: '=csSelect'
            },
            link: function (scope, element, attr, ctrl) {

                element.bind('change', function (evt) {
                    scope.$apply(function () {
                        ctrl.select(scope.row, 'multiple');
                    });
                });
                scope.$watch('row.isSelected', function (newValue, oldValue) {
                    if (newValue === true) {
                        element.parent().addClass('st-selected');
                    } else {
                        element.parent().removeClass('st-selected');
                    }
                    if($('input:checkbox:checked').length > 0) {
                        $("#bulkOperationsDropDown").removeClass("text-muted link-muted link-disabled");
                    } else {
                        $("#bulkOperationsDropDown").addClass("text-muted link-muted link-disabled");
                    }
                });
            }
        };
    });


    /**
     * jQuery functions
     **/

    /**
     * Set bulk operations as disabled if nothing is selected
     **/
    if($('input:checkbox:checked').length == 0) { $("#bulkOperationsDropDown").addClass("text-muted link-muted link-disabled"); }


    /**
     * Checkbox multi-select template with boolean
     * flag to disable selecting the very first row
     * flag might be required if a table cannot
     * have any null rows
     **/
    function checkBoxMultiSelectTemplate() {
        return '<input type="checkbox" value="[[row.id]]" />'
    }

    /**
     * Check all function
     **/
    $("#checkAll").change(function () {
        $("input:checkbox:not(:disabled)").prop('checked', $(this).prop("checked"));
        if($('input:checkbox:checked').length > 0) {
            $("#bulkOperationsDropDown").removeClass("text-muted link-muted link-disabled");
        } else {
            $("#bulkOperationsDropDown").addClass("text-muted link-muted link-disabled");
        }
    });

    /**
     * Reset wizard data or form data on modal close
     */
    $(".modal").on("hidden.bs.modal", function (e) {

        /** If modal has wizard **/

        if($(".modal").hasClass(".wizard"))
        {
            $(".modal").find(".wizard").wizard('selectedItem', { step: 1 });
            $(".wizard .form-group .form-control").val('');
            $(".wizard .form-group").removeClass('has-success').removeClass('has-error');
        }

        /** If modal has no wizard **/

        else
        {
            $(".form-group .form-control").val('');
            $(".form-group").removeClass('has-success').removeClass('has-error');
            $(".error-help-block").html('');
        }

    });


