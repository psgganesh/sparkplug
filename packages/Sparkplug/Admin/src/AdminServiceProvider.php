<?php

namespace Sparkplug\Admin;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class AdminServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/resources/assets/' => public_path('vendor/admin/assets/'),
        ], 'public');

        $this->publishes([
            __DIR__.'/resources/middleware/MustBeSuperAdministrator.php.stub' => app_path('/Http/Middleware/MustBeSuperAdministrator.php'),
        ], 'middleware');

        $this->loadTranslationsFrom(__DIR__.'/resources/lang', 'admin');

        $this->loadViewsFrom(__DIR__.'/resources/views/', 'admin');

        $this->registerHelpers();

        $this->setupRoutes($this->app->router);
    }

    /**
     * Define the routes for the application.
     *
     * @param Router|\Illuminate\Routing\Router $router
     */
    public function setupRoutes(Router $router)
    {
        $router->group(['namespace' => 'Sparkplug\Admin\Http\Controllers'], function($router)
        {
            require __DIR__.'/Http/routes.php';
        });
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerSkeleton();
    }

    /**
     * Register sparkplug view tag.
     *
     * @return void
     */
    private function registerSkeleton()
    {
        $this->app->bind('admin',function($app){
            return new AdminClass($app);
        });
    }

    /**
     * Register helpers file
     */
    public function registerHelpers()
    {
        require __DIR__.'/Helpers/helpers.php';
    }
}