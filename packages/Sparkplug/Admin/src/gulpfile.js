var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

    elixir.config.assetsDir = "resources/assets/sass/";
    mix.sass('app.scss');

    // Javascripts
    mix.copy('node_modules/angular/angular.min.js', 'resources/assets/js/angular.min.js');
    mix.copy('node_modules/jquery/dist/jquery.min.js', 'resources/assets/js/jquery.min.js');
    mix.copy('node_modules/angular-smart-table/dist/smart-table.min.js', 'resources/assets/js/smart-table.min.js');
    mix.copy('node_modules/angular-gravatar/build/angular-gravatar.min.js', 'resources/assets/js/angular-gravatar.min.js');
    mix.copy('node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js', 'resources/assets/js/bootstrap.min.js');
    mix.copy('node_modules/bootstrap-toggle/js/bootstrap-toggle.min.js', 'resources/assets/js/bootstrap-toggle.min.js');

    // Fonts
    mix.copy('node_modules/font-awesome/fonts/', 'public/fonts/');

    // Scripts
    mix.scripts([
        'jquery.min.js',
        'bootstrap.min.js',
        'angular.min.js',
        'smart-table.min.js',
        'angular-gravatar.min.js',
        'bootstrap-toggle.min.js'
    ], 'resources/assets/js/all.js', 'resources/assets/js');

    // Styles
    mix.styles([
        '/font-awesome/css/font-awesome.min.css',
        '/bootstrap-toggle/css/bootstrap-toggle.min.css'
    ], 'resources/assets/css/all.css', 'node_modules');


});
