<?php


function is_active($module) {

    if(is_array($module)) {
        foreach($module as $item) {
            return check_is_module($item);
        }
    } else {
        return check_is_module($module);
    }

    return null;
}

function check_is_module($namedroute) {

    return \Illuminate\Support\Facades\Request::route()->getName() == $namedroute ? 'active': '';

}