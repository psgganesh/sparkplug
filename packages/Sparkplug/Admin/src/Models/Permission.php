<?php

namespace Sparkplug\Admin\Models;

use Illuminate\Database\Eloquent\Model;


class Permission extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    public $guarded = ['id'];

    public $table = "permissions";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

//    protected $role;
//
//
//    public function __construct(Role $role)
//    {
//        $this->role = $role;
//    }

    /**
     * Get permissions grouped by role
     *
     */
    public function getAccessMatrix()
    {
        $grouped_permission = Permission::all();
        foreach ($grouped_permission as $selectedPermission )
        {
            $access_matrix_collection[$selectedPermission->id] = Role::leftJoin('role_has_permissions', function($join) {
                $join->on('roles.id', '=', 'role_has_permissions.role_id');
            })->get()->where('permission_id',$selectedPermission->id)->groupBy('role_id')->toArray();
        }

        return $access_matrix_collection;
    }
}
