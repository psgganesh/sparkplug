<?php

namespace Sparkplug\Admin\API\Transformers;

class AccessTransformer extends Transformer
{

    public function transform($item)
    {
        return [
            "id" => $item["id"],
            "name" => $item["name"]
        ];
    }

}