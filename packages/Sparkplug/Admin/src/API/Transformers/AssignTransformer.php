<?php

namespace Sparkplug\Admin\API\Transformers;

class AssignTransformer extends DisplayTableTransformer
{

    public function transform($item)
    {
        return [
            "id" => $item["id"],
            "name" => $item["name"],
            "email" => $item["email"],
            "role" => $item["role"]
        ];
    }

    public function columns()
    {
        return [
            "id" => 'increments',
            "email" => 'email',
            "name" => 'text',
            "role" => 'text'
        ];
    }

}