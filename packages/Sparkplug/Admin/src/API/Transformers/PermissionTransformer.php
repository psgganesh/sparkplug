<?php

namespace Sparkplug\Admin\API\Transformers;

class PermissionTransformer extends DisplayTableTransformer
{

    public function transform($item)
    {
        return [
            "id" => $item["id"],
            "name" => $item["name"]
        ];
    }

    public function columns()
    {
        return [
            "id" => 'increments',
            "name" => 'text'
        ];
    }

}