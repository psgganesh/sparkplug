<?php

namespace Sparkplug\Admin\API\Transformers;

class UserTransformer extends DisplayTableTransformer
{

    public function transform($item)
    {
        return [
            "id"            => $item["id"],
            "name"          => $item["name"],
            "email"         => $item["email"],
            "created_at"    => $item["created_at"],
            "updated_at"    => $item["updated_at"]
        ];
    }

    public function columns()
    {
        return [
            "id"            => 'increments',
            "email"         => 'email',
            "name"          => 'text',
            "created_at"    => 'timestamp',
            "updated_at"    => 'timestamp'
        ];
    }

}