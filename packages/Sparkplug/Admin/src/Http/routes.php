<?php


    // Entry-point route after successful login/register

    Route::group(['prefix' => '/superadmin', 'middleware' => ['web', 'auth', 'superadministrator'] ], function () {

        Route::get('/', ['as' => 'sparkplug', 'uses' => 'AdminController@index']);

        /*
        |--------------------------------------------------------------------------
        | ACL Routes
        |--------------------------------------------------------------------------
        |
        | This route group applies "ACL" this contains users, roles, permissions
        | and each of it's attributes / data-table.
        |
        */

        Route::get('/users', ['as' => 'users', 'uses' => 'AdminController@users'] );

        Route::get('/roles', ['as' => 'roles', 'uses' => 'AdminController@roles'] );

        Route::get('/permissions', ['as' => 'permissions', 'uses' => 'AdminController@permissions'] );

        Route::get('/assign', ['as' => 'assign', 'uses' => 'AdminController@assign'] );

        Route::get('/access', ['as' => 'access', 'uses' => 'AdminController@access'] );


        /*
         |--------------------------------------------------------------------------
         | API Routes
         |--------------------------------------------------------------------------
         |
         */

        Route::group(['prefix' => '/api/v1'], function () {

            Route::resource('/users', 'Rest\UsersController');

            Route::resource('/roles', 'Rest\RolesController');

            Route::resource('/permissions', 'Rest\PermissionsController');

            Route::resource('/assign', 'Rest\AssignController');

            Route::resource('/access', 'Rest\AccessController');

        });

    });