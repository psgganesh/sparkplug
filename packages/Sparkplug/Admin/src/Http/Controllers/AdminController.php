<?php

namespace Sparkplug\Admin\Http\Controllers;

use App\User;

use Sparkplug\Admin\Models\Role;
use Illuminate\Routing\Controller;
use Sparkplug\Admin\Models\Permission;
use Sparkplug\Admin\API\Transformers\AccessTransformer;
use Sparkplug\Admin\API\Transformers\AssignTransformer;
use Sparkplug\Admin\API\Transformers\UserTransformer;
use Sparkplug\Admin\API\Transformers\RoleTransformer;
use Sparkplug\Admin\API\Transformers\PermissionTransformer;

class AdminController extends Controller
{
    protected $user;
    protected $role;
    protected $data;
    protected $access;
    protected $permission;
    protected $userTransformer;
    protected $roleTransformer;
    protected $permissionTransformer;
    protected $assignTransformer;

    /**
     * Create a new controller instance.
     * @param User $user
     * @param Role $role
     * @param Permission $permission
     * @param UserTransformer $userTransformer
     * @param RoleTransformer $roleTransformer
     * @param PermissionTransformer $permissionTransformer
     * @param AssignTransformer $assignTransformer
     */
    public function __construct(User $user, Role $role, Permission $permission, UserTransformer $userTransformer, RoleTransformer $roleTransformer, PermissionTransformer $permissionTransformer, AssignTransformer $assignTransformer)
    {
        $this->user             = $user;
        $this->role             = $role;
        $this->permission       = $permission;
        $this->userTransformer  = $userTransformer;
        $this->roleTransformer  = $roleTransformer;
        $this->assignTransformer = $assignTransformer;
        $this->permissionTransformer  = $permissionTransformer;
        $this->data['trans']    = 'acl';
        $this->data['parent']   = 'navTitle';
        $this->data['children'] = ['users','roles','permissions', 'assign', 'access'];
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin::dashboard.index');
    }

    /**
     * View page for users module
     *
     * @return \Illuminate\Http\Response
     */
    public function users()
    {
        $this->data['model']      = $this->user;
        $this->data['columns']    = $this->userTransformer->columns();
        return view('admin::acl.users', $this->data);
    }

    /**
     * View page for role module
     *
     * @return \Illuminate\Http\Response
     */
    public function roles()
    {
        $this->data['model']      = $this->role;
        $this->data['columns']    = $this->roleTransformer->columns();
        return view('admin::acl.roles', $this->data);
    }

    /**
     * View page for permissions module
     *
     * @return \Illuminate\Http\Response
     */
    public function permissions()
    {
        $this->data['model']      = $this->permission;
        $this->data['columns']    = $this->permissionTransformer->columns();
        return view('admin::acl.permissions', $this->data);
    }


    /**
     * View page for assign module
     *
     * @return \Illuminate\Http\Response
     */
    public function assign()
    {
        $this->data['model']      = $this->user;
        $this->data['columns']    = $this->assignTransformer->columns();
        $this->data['roles']      = explode(',',$this->role->all()->implode('name', ','));
        return view('admin::acl.assign', $this->data);
    }


    /**
     * View page for access module
     *
     * @return \Illuminate\Http\Response
     */
    public function access()
    {
        return view('admin::acl.access', $this->data);
    }


}
