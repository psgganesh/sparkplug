<?php 
 
 namespace Sparkplug\Admin\Http\Controllers\Rest;

 use App\User;
 use Illuminate\Support\Facades\Auth;
 use Illuminate\Support\Facades\Artisan;
 use Sparkplug\Admin\Http\Requests\AssignRequest;
 use Sparkplug\Admin\Http\Controllers\ApiController;
 use Sparkplug\Admin\API\Transformers\AssignTransformer;

 class AssignController extends ApiController
 {

     protected $user;
     protected $assignTransformer;

     /**
      * PermissionsController constructor.
      * @param User $user
      * @param AssignTransformer $assignTransformer
      */
     public function __construct(User $user, AssignTransformer $assignTransformer)
     {
         $this->user = $user;
         $this->assignTransformer  = $assignTransformer;
     }

     /**
      * Display a listing of the resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function index()
     {
         $userId = Auth::id();
         $assign = $this->user->getUsersExceptLoggedInUserWithRoles($userId);
         return $this->respond([
             'data' => $this->assignTransformer->transformCollection($assign->toArray())
         ]);
     }

     public function store(AssignRequest $request)
     {
         $roleName = $request->roleName;
         $selectedUsers = $request->selectedUsers;
         $listIds = explode(",", $selectedUsers);
         switch($request->action)
         {
             case 'assign':
                 foreach($listIds as $item)
                 {
                     $this->revokeRole($roleName, $item);
                     $this->assignRole($roleName, $item);
                 }
                 break;
             case 'revoke':

                 break;
             default:
                 return response()->json(['result' => 'Nothing to do !'], 200);
         }
         return response()->json(['result' => 'Success'], 200);
     }

     public function revokeRole($roleName, $item)
     {
         Artisan::call('role:revoke', [
             '--role' => $roleName,
             '--email' => $item
         ]);
     }

     public function assignRole($roleName, $item)
     {
         Artisan::call('role:assign', [
             '--role' => $roleName,
             '--email' => $item
         ]);
     }


 }