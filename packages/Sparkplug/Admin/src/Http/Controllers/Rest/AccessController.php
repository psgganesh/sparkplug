<?php 
 
 namespace Sparkplug\Admin\Http\Controllers\Rest;

 use Sparkplug\Admin\Http\Controllers\ApiController;
 use Sparkplug\Admin\API\Transformers\AccessTransformer;
 use Sparkplug\Admin\Models\Permission;

 class AccessController extends ApiController
 {

     protected $permission;
     protected $accessTransformer;


     public function __construct(Permission $permission, AccessTransformer $accessTransformer)
     {
         $this->permission = $permission;
         $this->accessTransformer  = $accessTransformer;
     }

     public function index()
     {
         $access = $this->permission->getAccessMatrix();
         return $this->respond([
             'data' => $access
         ]);
     }

     public function store(AccessRequest $request)
     {

     }


     public function revokePermission($roleName, $item)
     {

     }

     public function assignPermission($roleName, $item)
     {

     }


 }