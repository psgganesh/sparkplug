<?php

namespace Sparkplug\Admin\Http\Controllers\Rest;

use App\User;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Sparkplug\Admin\Http\Controllers\ApiController;
use Sparkplug\Admin\Http\Requests\EditRolesRequest;
use Sparkplug\Admin\API\Transformers\RoleTransformer;
use Sparkplug\Admin\Http\Requests\CreateNewRolesRequest;

/**
 * Class UsersController
 * @package App\Http\Controllers\Rest
 */
class RolesController extends ApiController
{

    /**
     * @var Role
     */
    protected $role;

    /**
     * @var User
     */
    protected $user;

    /**
     * @var RoleTransformer
     */
    protected $roleTransformer;


    /**
     * UsersController constructor.
     * @param Role $role
     * @param User $user
     * @param RoleTransformer $roleTransformer
     */
    public function __construct(Role $role, User $user, RoleTransformer $roleTransformer)
    {
        $this->role = $role;
        $this->user = $user;
        $this->roleTransformer  = $roleTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = $this->role->all();
           
        return $this->respond([
            'data' => $this->roleTransformer->transformCollection($roles->toArray())
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateNewRolesRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateNewRolesRequest $request)
    {
        $this->role->firstOrCreate(
            [
                'name' => $request->name
            ]
        );

        session()->flash('message', trans('admin::admin.roles.addedNewRecord'));

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if( ! $this->role->find($id) )
        {
            return $this->respondNotFound("Role not found");
        }

        $role = $this->role->find($id);

        return $this->respond([
            'data' => $this->roleTransformer->transform($role)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EditRolesRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditRolesRequest $request, $id)
    {

        $this->role->where('id', $id)->update(
            [
                'name' => $request->name,
            ]
        );

        session()->flash('message', trans('admin::admin.roles.updatedRecord'));

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $currentUser = $this->user->find(Auth::id());
        $currentRole  = current(current($currentUser->roles));
        $listIds = explode(",", $id);
        foreach($listIds as $item)
        {
            if($currentRole->id != $item)
            {
                $this->role->destroy($item);
            }
        }

        return response()->json(['result' => 'Success'], 200);
    }

}
