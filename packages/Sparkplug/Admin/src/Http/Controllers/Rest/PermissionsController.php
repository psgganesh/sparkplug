<?php 
 
 namespace Sparkplug\Admin\Http\Controllers\Rest;

 use App\User;
 use Sparkplug\Admin\API\Transformers\PermissionTransformer;
 use Sparkplug\Admin\Http\Controllers\ApiController;
 use Sparkplug\Admin\Http\Requests\CreateNewPermissionsRequest;
 use Sparkplug\Admin\Http\Requests\EditPermissionsRequest;
 use Spatie\Permission\Models\Permission;

 class PermissionsController extends ApiController
 {

     /**
      * @var Permission
      */
     protected $permission;

     /**
      * @var User
      */
     protected $user;

     /**
      * @var PermissionTransformer
      */
     protected $permissionTransformer;

     /**
      * PermissionsController constructor.
      * @param Permission $permission
      * @param User $user
      * @param PermissionTransformer $permissionTransformer
      */
     public function __construct(Permission $permission, User $user, PermissionTransformer $permissionTransformer)
     {
         $this->permission = $permission;
         $this->user = $user;
         $this->permissionTransformer  = $permissionTransformer;
     }

     /**
      * Display a listing of the resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function index()
     {
         $permissions = $this->permission->all();

         return $this->respond([
             'data' => $this->permissionTransformer->transformCollection($permissions->toArray())
         ]);
     }

     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show($id)
     {
         if( ! $this->permission->find($id) )
         {
             return $this->respondNotFound("Role not found");
         }

         $permission = $this->permission->find($id);

         return $this->respond([
             'data' => $this->permissionTransformer->transform($permission)
         ]);
     }

     public function store(CreateNewPermissionsRequest $request)
     {

         $this->permission->firstOrCreate(
             [
                 'name' => $request->name
             ]
         );

         session()->flash('message', trans('admin::admin.permissions.addedNewRecord'));

         return back();
     }

     public function update(EditPermissionsRequest $request, $id)
     {

         $this->permission->where('id', $id)->update(
             [
                 'name' => $request->name
             ]
         );

         session()->flash('message', trans('admin::admin.permissions.updatedRecord'));

         return back();
     }

 }