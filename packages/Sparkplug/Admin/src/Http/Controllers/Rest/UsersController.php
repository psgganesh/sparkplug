<?php

namespace Sparkplug\Admin\Http\Controllers\Rest;

use App\User;
use Illuminate\Support\Facades\Auth;
use Sparkplug\Admin\Http\Requests\EditUsersRequest;
use Sparkplug\Admin\Http\Controllers\ApiController;
use Sparkplug\Admin\API\Transformers\UserTransformer;
use Sparkplug\Admin\Http\Requests\CreateNewUsersRequest;

/**
 * Class UsersController
 * @package App\Http\Controllers\Rest
 */
class UsersController extends ApiController
{

    /**
     * @var User
     */
    protected $user;

    /**
     * @var Auth
     */
    protected $auth;

    /**
     * @var UserTransformer
     */
    protected $userTransformer;

    /**
     * @var int
     */
    protected $defaultLoggedInUserId;

    /**
     * UsersController constructor.
     * @param User $user
     * @param Auth $auth
     * @param UserTransformer $userTransformer
     *
     */
    public function __construct(User $user, Auth $auth, UserTransformer $userTransformer)
    {
        $this->user = $user;
        $this->auth = $auth;
        $this->userTransformer  = $userTransformer;
        $this->defaultLoggedInUserId = 1;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userId = Auth::id();
        $users = $this->user->getUsersExceptLoggedInUser($userId);

        return $this->respond([
            'columns' => $this->userTransformer->columns(),
            'data' => $this->userTransformer->transformCollection($users->toArray())
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateNewUsersRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateNewUsersRequest $request)
    {

        $this->user->firstOrCreate(
            [
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password)
            ]
        );

        session()->flash('message', trans('admin::admin.users.addedNewRecord'));

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if( ! $this->isExistingUser($id) )
        {
            return $this->respondNotFound("User not found");
        }

        $user = $this->user->getUserInfoWithRole($id);

        return $this->respond([
            'data' => $this->userTransformer->transform($user)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EditUsersRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditUsersRequest $request, $id)
    {

        $this->user->where('id', $id)->update(
            [
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password)
            ]
        );

        session()->flash('message', trans('admin::admin.users.updatedRecord'));

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $listIds = explode(",", $id);
        foreach($listIds as $item)
        {
            $this->user->destroy($item);
        }

        return response()->json(['result' => 'Success'], 200);
    }

    public function isExistingUser($id)
    {
        if($this->user->find($id))
        {
            return true;
        }

        return false;
    }

    /**
     * Return logged in user's id, if logged in, else return the default user id
     *
     * @param $authObject
     * @return int
     */
    public function getLoggedInUser($authObject)
    {
        if ($authObject::check())
        {
            $this->defaultLoggedInUserId = $authObject::user()->id;
        }

        return $this->defaultLoggedInUserId;
    }

}
